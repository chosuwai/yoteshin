package com.example.yoteshin.data.vos;

public class GenresVOs {

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
