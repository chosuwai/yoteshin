package com.example.yoteshin.data.models;

import com.example.yoteshin.network.MoviesDataAgent;
import com.example.yoteshin.network.RetrofitDataAgentImpl;

public class PopularMoviesModel {

    private static PopularMoviesModel sObjInstance;
    private MoviesDataAgent mDataAgent;

    private PopularMoviesModel() {
        mDataAgent = RetrofitDataAgentImpl.getsObjInstance();
    }

    public static PopularMoviesModel getsObjInstance() {
        if (sObjInstance == null) {
            sObjInstance = new PopularMoviesModel();
        }
        return sObjInstance;
    }

    public void loadMoviesList() {
        mDataAgent.loadMoviesList(1);
    }

}
