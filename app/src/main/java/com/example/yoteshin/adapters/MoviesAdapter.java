package com.example.yoteshin.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yoteshin.R;
import com.example.yoteshin.data.vos.PopularMoviesResultsVO;
import com.example.yoteshin.delegates.MoviesDelegate;
import com.example.yoteshin.viewholders.MoviesViewHolder;

import java.util.ArrayList;
import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesViewHolder> {

    private List<PopularMoviesResultsVO> mMoviesList;
    private MoviesDelegate mMoviesDelegate;

    public MoviesAdapter(MoviesDelegate moviesDelegate) {
        mMoviesDelegate = moviesDelegate;
        mMoviesList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MoviesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.view_holder_movies, parent, false);
        return new MoviesViewHolder(view, mMoviesDelegate);
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesViewHolder holder, int position) {
        holder.bindData(mMoviesList.get(position));
    }


    @Override
    public int getItemCount() {
        return mMoviesList.size();
    }

    public void setResults(List<PopularMoviesResultsVO> moviesList) {
        mMoviesList = moviesList;
        notifyDataSetChanged();
    }
}
