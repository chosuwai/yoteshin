package com.example.yoteshin.events;

import com.example.yoteshin.data.vos.PopularMoviesResultsVO;

import java.util.List;

public class SuccessPopularMoviesEvent {

    private List<PopularMoviesResultsVO> results;

    public SuccessPopularMoviesEvent(List<PopularMoviesResultsVO> results) {
        this.results = results;
    }

    public List<PopularMoviesResultsVO> getResults() {
        return results;
    }
}
