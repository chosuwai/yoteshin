package com.example.yoteshin.viewholders;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class UpcomingMoviesViewHolder extends RecyclerView.ViewHolder {

    public UpcomingMoviesViewHolder(@NonNull View itemView) {
        super(itemView);
    }
}
