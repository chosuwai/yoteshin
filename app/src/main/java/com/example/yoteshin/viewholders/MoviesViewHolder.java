package com.example.yoteshin.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yoteshin.R;
import com.example.yoteshin.data.vos.PopularMoviesResultsVO;
import com.example.yoteshin.delegates.MoviesDelegate;
import com.example.yoteshin.utils.GlideApp;
import com.example.yoteshin.utils.MoviesConstants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_movie_poster)
    ImageView ivMoviePoster;

    @BindView(R.id.tv_movie_title)
    TextView tvMovieTitle;

    @BindView(R.id.tv_genres)
    TextView tvGenres;

    @BindView(R.id.tv_rating)
    TextView tvRating;

    @BindView(R.id.tv_vote_count)
    TextView tvVoteCount;

    private PopularMoviesResultsVO mMovies;
    private MoviesDelegate mMoviesDelegate;

    public MoviesViewHolder(@NonNull View itemView, MoviesDelegate moviesDelegate) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        mMoviesDelegate=moviesDelegate;
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMoviesDelegate.onTapMovie();
            }
        });
    }

    public void bindData(PopularMoviesResultsVO movies) {
        mMovies = movies;

        GlideApp.with(ivMoviePoster.getContext())
                .load(appendImageString(movies.getPosterPath()))
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error_loading)
                .into(ivMoviePoster);

        tvMovieTitle.setText(movies.getTitle());
        tvVoteCount.setText(tvVoteCount.getContext().getResources().getString(R.string.format_vote_count, movies.getVoteCount()));//TODO find error
        tvRating.setText(String.valueOf((movies.getVoteAverage()) / 2));//TODO one decimal place

    }

    private String appendImageString(String imageUrl) {
        String fullImageUrl;
        fullImageUrl = MoviesConstants.IMAGE_ROOT_DIR + imageUrl;
        return fullImageUrl;
    }

}
