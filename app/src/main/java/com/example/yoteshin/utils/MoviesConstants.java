package com.example.yoteshin.utils;

public class MoviesConstants {

    public static final String API_BASE_URL="https://api.themoviedb.org/3/";
    public static final String POPULAR_MOVIES_END_POINT="movie/popular?api_key=ecfc9c3b820fbc659ba3f784ca695adf";
    public static final String GENRES_END_POINT="genre/movie/list?api_key=ecfc9c3b820fbc659ba3f784ca695adf";

    public static final String IMAGE_ROOT_DIR="https://image.tmdb.org/t/p/w500";

    public static final String PARAM_PAGE="page";

}
