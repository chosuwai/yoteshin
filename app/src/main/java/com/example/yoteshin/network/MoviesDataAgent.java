package com.example.yoteshin.network;

public interface MoviesDataAgent {

    void loadMoviesList(int page);

}
