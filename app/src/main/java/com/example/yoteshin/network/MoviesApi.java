package com.example.yoteshin.network;

import com.example.yoteshin.network.responses.PopularMoviesResponse;
import com.example.yoteshin.utils.MoviesConstants;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface MoviesApi {

    @FormUrlEncoded
    @POST(MoviesConstants.POPULAR_MOVIES_END_POINT)
    Call<PopularMoviesResponse> loadMoviesList(
            @Field(MoviesConstants.PARAM_PAGE) int page
    );

}
