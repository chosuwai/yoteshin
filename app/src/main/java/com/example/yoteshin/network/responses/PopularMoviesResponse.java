package com.example.yoteshin.network.responses;

import com.example.yoteshin.data.vos.PopularMoviesResultsVO;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class PopularMoviesResponse {

    @SerializedName("page")
    private int page;

    @SerializedName("results")
    private List<PopularMoviesResultsVO> results;

    @SerializedName("total_pages")
    private int totalPages;

    @SerializedName("total_results")
    private int totalResults;

    @SerializedName("status_message")
    private String statusMessage;

    public int getPage() {
        return page;
    }

    public List<PopularMoviesResultsVO> getResults() {
        return results;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public boolean isResponseOk() {
        return (results != null);
    }
}
