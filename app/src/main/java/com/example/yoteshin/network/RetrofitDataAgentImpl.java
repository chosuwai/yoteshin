package com.example.yoteshin.network;

import com.example.yoteshin.data.models.PopularMoviesModel;
import com.example.yoteshin.events.ApiErrorEvent;
import com.example.yoteshin.events.SuccessPopularMoviesEvent;
import com.example.yoteshin.network.responses.PopularMoviesResponse;
import com.example.yoteshin.utils.MoviesConstants;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitDataAgentImpl implements MoviesDataAgent {

    private static RetrofitDataAgentImpl sObjInstance;
    private MoviesApi mTheApi;

    private RetrofitDataAgentImpl() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MoviesConstants.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        mTheApi = retrofit.create(MoviesApi.class);
    }

    public static RetrofitDataAgentImpl getsObjInstance() {
        if (sObjInstance == null) {
            sObjInstance = new RetrofitDataAgentImpl();
        }
        return sObjInstance;
    }

    @Override
    public void loadMoviesList(int page) {
        Call<PopularMoviesResponse> loadMoviesCall = mTheApi.loadMoviesList(page);
        loadMoviesCall.enqueue(new Callback<PopularMoviesResponse>() {
            @Override
            public void onResponse(Call<PopularMoviesResponse> call, Response<PopularMoviesResponse> response) {
                PopularMoviesResponse moviesResponse = response.body();
                if (moviesResponse != null && moviesResponse.isResponseOk()) {
                    SuccessPopularMoviesEvent event = new SuccessPopularMoviesEvent(moviesResponse.getResults());
                    EventBus.getDefault().post(event);
                } else {
                    if (moviesResponse == null) {
                        ApiErrorEvent event = new ApiErrorEvent("Empty response in network call.");
                        EventBus.getDefault().post(event);
                    } else {
                        ApiErrorEvent event = new ApiErrorEvent(moviesResponse.getStatusMessage());
                        EventBus.getDefault().post(event);
                    }
                }
            }

            @Override
            public void onFailure(Call<PopularMoviesResponse> call, Throwable t) {
                ApiErrorEvent event = new ApiErrorEvent(t.getMessage());
                EventBus.getDefault().post(event);
            }
        });
    }
}
