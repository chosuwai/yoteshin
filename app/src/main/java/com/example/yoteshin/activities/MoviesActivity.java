package com.example.yoteshin.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yoteshin.R;
import com.example.yoteshin.adapters.MoviesAdapter;
import com.example.yoteshin.data.models.PopularMoviesModel;
import com.example.yoteshin.delegates.MoviesDelegate;
import com.example.yoteshin.events.SuccessPopularMoviesEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesActivity extends BaseActivity
        implements MoviesDelegate {

    @BindView(R.id.rv_movies)
    RecyclerView rvMovies;

    private MoviesAdapter mMoviesAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);
        ButterKnife.bind(this, this);

        mMoviesAdapter = new MoviesAdapter(this);
        rvMovies.setAdapter(mMoviesAdapter);
        rvMovies.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.VERTICAL, false));

        PopularMoviesModel.getsObjInstance().loadMoviesList();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSuccessPopularMovies(SuccessPopularMoviesEvent event) {
        mMoviesAdapter.setResults(event.getResults());
    }

    @Override
    public void onTapMovie() {
        Intent intent = new Intent(getApplicationContext(), MovieDetailsActivity.class);
        startActivity(intent);
    }
}
