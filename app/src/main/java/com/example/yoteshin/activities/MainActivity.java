package com.example.yoteshin.activities;

import android.content.Intent;
import android.os.Bundle;

import com.example.yoteshin.R;
import com.google.android.material.snackbar.Snackbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import javax.security.auth.login.LoginException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @BindView(R.id.tv_register)
    TextView btnRegister;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_password)
    EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this, this);

        btnRegister.setOnClickListener(v -> {
            Log.d("MainActivity", "User tap Register to navigate screen.");

            Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
            startActivity(intent);
        });

        btnLogin.setOnClickListener(v -> {
            Log.d("MainActivity", "User taps Login button.");

            String email = etEmail.getText().toString();
            if (TextUtils.isEmpty(email)) {
                etEmail.setError("Enter your email here.");
                return;
            }

            String password = etPassword.getText().toString();
            if (TextUtils.isEmpty(password)) {
                etPassword.setError("Enter your password here");
                return;
            }

            if (TextUtils.equals(email, "admin")
                    && TextUtils.equals(password, "123")) {
                //Snackbar.make(v, "Login Success.", Snackbar.LENGTH_INDEFINITE).show();

                Intent intent = new Intent(getApplicationContext(), MoviesActivity.class);
                startActivity(intent);

            } else {
                if (!email.equals("chosuwai@gmail.com") & password.equals("123")) {
                    Toast.makeText(v.getContext(), "Incorrect Email", Toast.LENGTH_SHORT).show();
                } else if (email.equals("chosuwai@gmail.com") & !password.equals("123")) {
                    Toast.makeText(v.getContext(), "Incorrect password", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(v.getContext(), "Both Email and Password are incorrect", Toast.LENGTH_LONG).show();
                }
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}