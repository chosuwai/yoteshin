package com.example.yoteshin.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yoteshin.R;
import com.example.yoteshin.adapters.UpcomingMoviesAdapter;
import com.example.yoteshin.delegates.MoviesDelegate;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieDetailsActivity extends BaseActivity {

    @BindView(R.id.rv_upcoming_movies)
    RecyclerView rvUpcomingMovies;

    private UpcomingMoviesAdapter mUpcomingMoviesAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        ButterKnife.bind(this,this);

        mUpcomingMoviesAdapter = new UpcomingMoviesAdapter();
        rvUpcomingMovies.setAdapter(mUpcomingMoviesAdapter);
        rvUpcomingMovies.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.HORIZONTAL, false));
    }

}
